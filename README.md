# GitLab 12.10 Auto DevOps and Secure CI templates

This is an extraction of CI templates as of GitLab 12.10.

This allows you to use these templates if updating your `.gitlab-ci.yml` to use `rules`
is not possible.
(In GitLab 13.0, [Auto DevOps and Secure CI templates transition to using `rules`](https://gitlab.com/groups/gitlab-org/-/epics/2300))

## How to use

You can use these templates in a few ways

- Via `include:remote`
- Setting up a new project and referencing via `include:local`

### Include using `include:remote`

1. Update your `.gitlab-ci.yml` file to refer to the 12.10 template that is hosted on GitLab.com

    ```yml
    include:
      - remote: 'https://gitlab.com/gitlab-org/auto-devops-v12-10/-/raw/master/Auto-DevOps-remote.gitlab-ci.yml'
    ```

### Include using a new project, and using `include:local`

This option is useful for users who do not wish to use `include:remote`.

1. Create a new project that will have the templates from this project. On your GitLab instance, go to New Project >> Import Project.
   Select Repo by URL option, and enter
   `https://gitlab.com/gitlab-org/auto-devops-v12-10.git` as the Git repository URL.
1. Update your `.gitlab-ci.yml` file to refer to the 12.10 template.

    ```yml
    include:
      - project: 'my-group/my-project'
        file: 'Auto-DevOps.gitlab-ci.yml'
    ```
